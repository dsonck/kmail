# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kmail package.
# Luigi Toscano <luigi.toscano@tiscali.it>, 2019, 2020, 2021, 2022, 2023, 2024.
#
msgid ""
msgstr ""
"Project-Id-Version: kmail\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-04-04 00:39+0000\n"
"PO-Revision-Date: 2024-01-02 19:26+0100\n"
"Last-Translator: Luigi Toscano <luigi.toscano@tiscali.it>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Luigi Toscano"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "luigi.toscano@tiscali.it"

#: main.cpp:28 main.cpp:30
#, kde-format
msgid "KMail Assistant for refreshing settings"
msgstr "Assistente di KMail per aggiornare le impostazioni"

#: main.cpp:32
#, kde-format
msgid "(c) 2019-2024 Laurent Montel <montel@kde.org>"
msgstr "(c) 2019-2024 Laurent Montel <montel@kde.org>"

#: main.cpp:33
#, kde-format
msgid "Laurent Montel"
msgstr "Laurent Montel"

#: main.cpp:33
#, kde-format
msgid "Author"
msgstr "Author"

#: refreshsettingsassistant.cpp:22
#, kde-format
msgctxt "@title:window"
msgid "KMail Refresh Settings"
msgstr "Rinnovo delle impostazioni di KMail"

#: refreshsettingsassistant.cpp:39
#, kde-format
msgid "Warning"
msgstr "Avviso"

#: refreshsettingsassistant.cpp:43
#, kde-format
msgid "Clean up Settings"
msgstr "Pulisci le impostazioni"

#: refreshsettingsassistant.cpp:47
#, kde-format
msgid "Finish"
msgstr "Termina"

#: refreshsettingscleanuppage.cpp:22
#, kde-format
msgid "Clean"
msgstr "Pulisci"

#: refreshsettingscleanuppage.cpp:52
#, kde-format
msgid "Remove obsolete \"TipOfDay\" settings: Done"
msgstr ""
"Rimozione delle impostazioni dell'obsoleto suggerimento del giorno: "
"completata"

#: refreshsettingscleanuppage.cpp:64
#, kde-format
msgid "Delete Dialog settings in file `%1`: Done"
msgstr "Rimozione delle impostazioni delle finestre nel file «%1»: completata"

#: refreshsettingscleanuppage.cpp:76
#, kde-format
msgid "Delete Filters settings in file `%1`: Done"
msgstr "Rimozione delle impostazioni dei filtri nel file «%1»: completata"

#: refreshsettingscleanuppage.cpp:92
#, kde-format
msgid "Clean Folder Settings in setting file `%1`: Done"
msgstr ""
"Pulizia delle impostazioni delle cartelle nel file di configurazione «%1»: "
"completata"

#: refreshsettingscleanuppage.cpp:148
#, kde-format
msgid "Clean Dialog Size in setting file `%1`: Done"
msgstr ""
"Pulizia delle dimensioni della finestra nel file di configurazione «%1»: "
"completata"

#: refreshsettingsfirstpage.cpp:20
#, kde-format
msgid "Please close KMail/Kontact before using it."
msgstr "Chiudere KMail/Kontact prima di usarlo."
